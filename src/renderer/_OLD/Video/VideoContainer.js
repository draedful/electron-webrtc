import EventEmitter from 'events';

export default class VideoContainer {

	constructor(node) {
		this.node = node;
	}

	setMain(isMain) {
		this.main = isMain;
		this.node.classList.toggle('video-main', this.main);
	}

}