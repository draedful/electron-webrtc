import LocalVideoSelector from './Components/LocalCameraSelector';
import DescropCapturer from './Components/DesctopCapturers';

import { LocalStreamManager } from './Services/StreamManager';
import DeviceManager from './Services/DeviceManager';
import LocalVideo from './Components/LocalVideoStream/VideoWrapper';
import MessageChannel from './MessageChannel';



const localSelector = new LocalVideoSelector(document.querySelector('#videoInput'));
const screenSelector = new DescropCapturer(document.querySelector('#screenInput'));

const createStream = (value) => {
	DeviceManager
		.getMediaStreamFrom(value)
		.then((stream) => LocalStreamManager.setStream(stream));
};

localSelector.onSelect(createStream);
screenSelector.onSelect(createStream);

const localVideo = new LocalVideo(document.querySelector('#videowrapper'));

localVideo.onChange(() => {
	const size = localVideo.getSize();
	changeSize(+size.width, +size.height)
});

window.requestAnimationFrame(() => {
	DeviceManager.getMediaStream()
		.then((stream) => LocalStreamManager.setStream(stream));
});


