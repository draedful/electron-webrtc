export const INVITE = 'invite';
export const ACCEPT = 'accept';
export const READY = 'ready';
export const CANDIDATE = 'candidate';
export const OFFER = 'offer';
export const ANSWER = 'answer';
export const CLOSE = 'endcall';

export const send = sendMainMessage;

export const sendInvite = () => send({
	messageType: INVITE
});

export const acceptCall = () => send({
	messageType: ACCEPT
});

export const sendReady = () => send({
	messageType: READY
});

export const sendSessionDescription = (descriptionType, sessionDescription) => {
	send({
		messageType: descriptionType,
		sessionDescription: sessionDescription.toJSON ? sessionDescription.toJSON() : sessionDescription,
	});
};


export const sendOfferDescription = (sessionDescription) => sendSessionDescription(OFFER, sessionDescription);
export const sendAnswerDescription = (sessionDescription) => sendSessionDescription(ANSWER, sessionDescription);

export const sendCandidate = (candidate, index, mediaId) => {
	send({
		messageType: CANDIDATE,
		candidate,
		index,
		mediaId
	});
};
