import DeviceManager from '../../Devices';
import { LocalStreamManager } from '../../Services/StreamManager/index';
import VideoController from './VideoController';
import debounce from 'lodash/debounce';

export default class LocalVideoController extends VideoController {

	constructor(node) {
		super(node);

		const changeSizeWindow = () => window.requestAnimationFrame(() => {
			changeSize(+this.videoNode.videoWidth, +this.videoNode.videoHeight)
		});

		this.videoNode.addEventListener('loadeddata', changeSizeWindow);

		LocalStreamManager.onChange(() => this.loadStream());
	}

	loadStream() {
		DeviceManager.getMediaStream()
			.then((stream) => this.setStream(stream))
	}

}