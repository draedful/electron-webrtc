import EventEmitter from 'events';

const SELECT = Symbol('select');

export default class SelectEmitter extends EventEmitter {

	emitSelect(value) {
		return this.emit(SELECT, value);
	}

	onSelect(cb) {
		return this.on(SELECT, cb);
	}

	offSelect(cb) {
		return this.off(SELECT, cb);
	}

}