
import DeviceManager from '../../Devices';
import DeviceSelector from './Selector';

export default class AudioInputSelector extends DeviceSelector {

	constructor(node) {
		super(node);
		DeviceManager.onUpdate(() => this.updateAudioDevices());
	}

	onSelect(deviceId) {
		DeviceManager.setAudioOutputDevice(deviceId === 'off_audio' ? null : deviceId);
	}

	updateAudioDevices() {
		return DeviceManager.getAudioDevices()
			.then(devices => {
				this.updateDevicesView(devices);
				return devices;
			})
			.then(() => DeviceManager.getAudioDeviceId())
			.then((device) => this.setSelectedDevice(device))
	}

}