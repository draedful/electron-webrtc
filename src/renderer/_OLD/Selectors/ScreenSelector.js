import DeviceManager from '../../Services/DeviceManager/index';
import Selector from './Selector';


export default class ScreenSelector extends Selector {

	constructor(node) {
		super(node);
		node.addEventListener('click', () => {
			this.clear();
			DeviceManager.getScreens()
				.then((sources) => this.updateDevicesView(sources))
		});
	}

	onSelect(deviceId) {
		DeviceManager.getMediaStreamFrom([deviceId, DeviceManager.defaultVideoDeviceId])
			.then((stream) => {
				window.globalStream = stream;
			})
	}

	addDevice(device) {
		const option = new Option(device.name, device.id);
		if (device.deviceId === this.selectedDeviceId) {
			option.selected = true;
		}
		this.nodeDeviceSelector.options.add(option);
		return this;
	}

}