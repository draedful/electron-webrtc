import EventEmitter from 'events';

export const SELECT = Symbol('select');

export default class SelectEmitter extends EventEmitter {

  emitSelect(value) {
    return this.emit.apply(this, Array.from(arguments).unshift(SELECT));
  }

  onSelect(cb) {
    return this.on(SELECT, cb);
  }

}