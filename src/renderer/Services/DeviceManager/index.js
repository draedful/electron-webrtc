// @flow
import EventListener from 'events';

import { desktopCapturer, screen } from 'electron';

const getAllDevices = () => navigator.mediaDevices.enumerateDevices()

const getSize = () => screen.getPrimaryDisplay().workAreaSize;

const AUDIO_INPUT_KIND = 'audioinput';
const AUDIO_OUTPUT_KIND = 'audioinput';
const VIDEO_INPUT_KIND = 'videoinput';
const VIDEO_OUTPUT_KIND = 'videoinput';

const isDevice = (device) => !!(device.kind && device.deviceId);

class DeviceManager extends EventListener {

	constructor() {
		super();
		this.devices = new Map();
		this.screens = new Map();

		this.defaultAudioDeviceId = null;
		this.defaultVideoDeviceId = null;

		navigator.mediaDevices.addEventListener('devicechange', () => this.fetchDevices().then(() => this.emit('UPDATE')));
	}

	getDevices() {
		if (this.devices.size) {
			return Promise.resolve(this.devices);
		}
		return this.fetchDevices();
	}

	getVideoDevices() {
		return this.getDevices()
			.then((devices) => {
				const video = [];
				devices.forEach((device) => {
					if (device.kind === VIDEO_INPUT_KIND) {
						video.push(device);
					}
				});
				return video;
			})
	}

	getAudioDevices() {
		return this.getDevices()
			.then((devices) => {
				const audio = [];
				devices.forEach((device) => {
					if (device.kind === AUDIO_INPUT_KIND) {
						audio.push(device);
					}
				});
				return audio;
			})
	}

	getVideoDeviceId() {
		if (this.defaultVideoDeviceId && this.devices.has(this.defaultVideoDeviceId)) {
			return Promise.resolve(this.defaultVideoDeviceId);
		}
		return this.fetchDevices().then(() => this.defaultVideoDeviceId);
	}

	getVideoDevice() {
		return this.getVideoDeviceId()
			.then(id => this.devices.get(id));
	}


	getAudioDeviceId() {
		if (this.defaultAudioDeviceId && this.devices.has(this.defaultAudioDeviceId)) {
			return Promise.resolve(this.defaultAudioDeviceId);
		}
		return this.fetchDevices().then(() => this.defaultAudioDeviceId);
	}

	getAudioDevice() {
		return this.getAudioDeviceId()
			.then(id => this.devices.get(id));
	}

	setVideoOutputDevice(device) {
		let videoDevice;
		if (device === null) {
			this.defaultVideoDeviceId = null;
			this.emit('VIDEO_UPDATE');
		} else {
			if (isDevice(device) && this.devices.has(device.deviceId)) {
				videoDevice = device;
			} else if (this.devices.has(device)) {
				videoDevice = this.devices.get(device);
			}
			if (videoDevice) {
				this.defaultVideoDeviceId = videoDevice.deviceId;
				this.emit('VIDEO_UPDATE');
			}
		}
	}

	setAudioOutputDevice(device) {
		let audioDevice;
		if (device === null) {
			this.defaultAudioDeviceId = null;
			this.emit('AUDIO_UPDATE');
		} else {
			if (isDevice(device) && this.devices.has(device.deviceId)) {
				audioDevice = device;
			} else if (this.devices.has(device)) {
				audioDevice = this.devices.get(device);
			}
			if (audioDevice) {
				this.defaultAudioDeviceId = audioDevice.deviceId;
				this.emit('AUDIO_UPDATE');
			}
		}

	}

	hasVideo() {
		return this.getVideoDeviceId().then((deviceId) => !!deviceId);
	}

	hasAudio() {
		return this.getAudioDeviceId().then((deviceId) => !!deviceId);
	}

	getScreens() {
		return new Promise((resolve, reject) => {
			desktopCapturer.getSources({ types: ['window', 'screen'] }, (error, sources) => {
				if (error) {
					return reject(error);
				}
				return resolve(sources);
			}, reject);
		})
	}

	fetchDevices() {
		return getAllDevices()
			.then((devices) => {
				devices.forEach((device) => this.devices.set(device.deviceId, device));
				return devices;
			})
			.then((devices) => {
				if (!this.defaultAudioDeviceId || !this.devices.has(this.defaultAudioDeviceId)) {
					const audioDevice = devices.find((device) => device.kind === AUDIO_INPUT_KIND);
					if (audioDevice) {
						this.defaultAudioDeviceId = audioDevice.deviceId;
					}
				}

				if (!this.defaultVideoDeviceId || !this.devices.has(this.defaultVideoDeviceId)) {
					const videoDevice = devices.find((device) => device.kind === VIDEO_INPUT_KIND);
					if (videoDevice) {
						this.defaultVideoDeviceId = videoDevice.deviceId;
					}
				}

			})
			.then(() => this.devices);
	}

	getMediaStreamFrom(...devicesId) {
		return Promise.all(
			devicesId.map((deviceId) => this.getDeviceStream(deviceId))
		)
			.then((streams) => {
				return streams.reduce((bigStream, stream) => {
					stream.getTracks().forEach((track) => {
						bigStream.addTrack(track);
						track.stop();
					});
					return bigStream;
				});
			});
	}

	getdefaultStream() {

	}

	getDeviceStream(deviceId) {
		const config = {};
		const device = this.devices.get(deviceId);
		if (device) {
			if (device.kind === AUDIO_INPUT_KIND) {
				config.video = false;
				config.audio = {
					deviceId: { exact: device.deviceId },
					echoCancellation: true
				}

			} else if (device.kind === VIDEO_INPUT_KIND) {
				config.audio = false;
				config.video = {
					deviceId: { exact: device.deviceId },
				}
			}
		} else {
			const maxSize = getSize();
			config.audio = false;
			config.video = {
				mandatory: {
					chromeMediaSource: 'desktop',
					chromeMediaSourceId: deviceId,
					maxWidth: maxSize.width,
					maxHeight: maxSize.height
				}
			}
		}
		return navigator.mediaDevices.getUserMedia(config)

	}

	getMediaStream(videoConfig = {}, audioConfig = {}) {
		return this.getDevices()
			.then(() => {
				if (this.defaultAudioDeviceId) {
					audioConfig.deviceId = { exact: this.defaultAudioDeviceId };
					if (this.defaultVideoDeviceId) {
						const device = this.devices.get(this.defaultVideoDeviceId);
						if (device.type === 'screen') {
							audioConfig = {
								mandatory: {
									chromeMediaSource: 'desktop'
								}
							};
							const minSize = device.preview.getSize();
							const maxSize = getSize();
							videoConfig = {
								mandatory: {
									chromeMediaSource: 'desktop',
									chromeMediaSourceId: device.deviceId,
									minWidth: minSize.width,
									maxWidth: maxSize.width,
									minHeight: minSize.height,
									maxHeight: maxSize.height
								}
							}
						} else {
							videoConfig.deviceId = { exact: this.defaultVideoDeviceId };
						}
					}
					const MediaConfig = {
						audio: audioConfig,
						video: this.defaultVideoDeviceId ? videoConfig : false
					};

					return navigator.mediaDevices.getUserMedia(MediaConfig)
				}
				return Promise.reject();
			});
	}

	onAudioChange(cb) {
		this.on('AUDIO_UPDATE', cb);
	}

	onVideoChange(cb) {
		this.on('VIDEO_UPDATE', cb);
	}

	onUpdate(cb) {
		this.on('UPDATE', cb);
	}

}
/**/

export default new DeviceManager();