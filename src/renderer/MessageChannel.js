
import EventEmitter from 'events';


class MessageChannel extends EventEmitter {

	constructor() {
		super();
		onMainMessage((message) => {
			console.log(message);
			this.emit('Message', message);
			this.emit(message.messageType, message);
		})
	}

	onMessage(cb) {
		this.on('Message', cb);
	}

	send(message) {
		sendMainMessage(message);
	}

}

export default new MessageChannel();