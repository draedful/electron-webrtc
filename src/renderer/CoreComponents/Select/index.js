import SelectEmitter from './SelectEmitter';

export default class Select extends SelectEmitter {

	constructor(node) {
		super();
		this.selectorNode = node;
		this.selectorNode.addEventListener('change', (e) => this.emitSelect(e.target.value));
	}

	addOption(optionName, optionValue) {
		const option = new Option(optionName, optionValue);
		this.selectorNode.options.add(option);
		return this;
	}

	getSelected() {
		return this.selectorNode.options.item(this.selectorNode.selectedIndex);
	}

	getValue() {
	  return this.getSelected().value;
  }

	clear() {
		this.selectorNode.innerHTML = "";
		return this;
	}

}