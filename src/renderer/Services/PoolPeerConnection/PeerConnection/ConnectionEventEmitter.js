import EventEmitter from 'events';

export const CONNECT = Symbol('CONNECT');
export const CHANGE_STREAM = Symbol('CHANGE_STREAM');
export const CLOSE = Symbol('CLOSE');
export const ICE_CANDIDATE = Symbol('ICE_CANDIDATE');

export default class PerrConnectionEventEmitter extends EventEmitter {

	onConnect(cb) {
		return this.on(CONNECT, cb);
	}

	offConnect(cb) {
		return this.off(CONNECT, cb);
	}

	onIceCandidate(cb) {
		return this.on(ICE_CANDIDATE, cb);
	}

	offIceCandidate(cb) {
		return this.off(ICE_CANDIDATE, cb);
	}

	onChangeStream(cb) {
		return this.on(CHANGE_STREAM, cb);
	}

	offChangeStream(cb) {
		return this.off(CHANGE_STREAM, cb);
	}

	onClose(cb) {
		return this.on(CLOSE, cb);
	}

	offClose(cb) {
		return this.off(CLOSE, cb);
	}

	destroy() {
		[CONNECT, CLOSE, ICE_CANDIDATE, CHANGE_STREAM].forEach((eventName) => {
			this.removeAllListeners(eventName);
		})
	}

}