import EventEmitter from 'events';

export const CHANGE = Symbol('CHANGE');


export class StreamManager extends EventEmitter {

	constructor() {
		super();
		this.stream = null;
	}

	setStream(stream) {
		this.stream = stream;
		this.emit(CHANGE, this.stream);
	}

	onChange(cb) {
		return this.on(CHANGE, cb);
	}

	offChange(cb) {
		return this.on(CHANGE, cb);
	}

}

export const LocalStreamManager = new StreamManager();