import VideoComponent from '../../CoreComponents/Video';
import isString from 'lodash/isString';
import { LocalStreamManager } from '../../Services/StreamManager';

export default class VideoWrapper extends VideoComponent {

	constructor(wrapperNode) {
		const videoComponent = wrapperNode.querySelector('video');
		if (!videoComponent) {
			throw new Error('wrapper should contains video');
		}
		super(videoComponent);
		LocalStreamManager.onChange((stream) => this.setStream(stream));
	}

	static createVideoWrapper(parent, markup, wrapperClass) {
		const wrapper = document.createElement('div');
		if(wrapperClass) {
			wrapper.classList.add(wrapperClass);
		}
		if (isString(markup)) {
			wrapper.innerHTML = markup;
		}
		parent.appendChild(wrapper);
		return new VideoWrapper(wrapper);
	}

	getSize() {
		return {
			width: this.videoNode.videoWidth,
			height: this.videoNode.videoHeight
		}
	}

}