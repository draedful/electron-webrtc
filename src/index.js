import { app, BrowserWindow, ipcMain, screen } from 'electron';
import path from 'path';

const windows = new Map();

const startApp = () => {
	const w1 = createWindow();
	windows.set(w1.id, w1);
	/*const w2 = createWindow();
	 windows.set(w2.id, w2);*/
};

const sendMessage = (message, window) => {
	windows.forEach((win, id) => {
		if (id !== window.id) {
			win.webContents.send('message', message)
		}
	});
};

const minWidth = 320;
const minHeight = 240;

const setSize = (window, width, height) => {

	const windowPosition = window.getBounds();
	const windowDisplay = screen.getDisplayMatching(windowPosition);
	const displaySize = windowDisplay.workAreaSize;

	const maxWidthSize = displaySize.width - minWidth;
	const maxHeightSize = displaySize.height - minHeight;

	const widthSize = minWidth > width ? minWidth : width >= displaySize.width ? maxWidthSize : width;
	const heightSize = minHeight > height ? minHeight : height >= displaySize.height ? maxHeightSize : height;

	const x = (windowPosition.x + (windowPosition.width / 2)) - (widthSize / 2);
	const y = (windowPosition.y + (windowPosition.height / 2)) - (heightSize / 2);

	window.setAspectRatio(widthSize / heightSize);
	window.setBounds({ width: widthSize, height: heightSize, x, y }, true);

};

const createWindow = () => {
	// Create the browser window.
	const mainWindow = new BrowserWindow({
		width: minWidth,
		height: minHeight,
		minWidth,
		minHeight,
		webPreferences: {
			webSecurity: false,
			preload: path.join(__dirname, 'renderer/preload.js')
		},
		draggable: true,
		transparent: true,
		acceptFirstMouse: true,

	});

	/*mainWindow.setContentProtection(true);*/
	// and load the index.html of the app.
	mainWindow.loadURL(`file://${__dirname}/renderer/index.html`);

	mainWindow.setContentProtection(true);
	mainWindow.setVibrancy('popover');

	ipcMain.on('message', (e, message) => {
		if (e.sender.id !== mainWindow.id) {
			sendMessage(message, e.sender);
		}
	});

	ipcMain.on('manage', (e, message) => {
		switch (message.type) {
			case 'setSize':
				return setSize(mainWindow, message.width, message.height, message.aspectRatio);
		}
	});

	return mainWindow;
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', startApp);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
	// On OS X it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	// On OS X it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (windows.length === 0) {
		startApp();
	}
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
