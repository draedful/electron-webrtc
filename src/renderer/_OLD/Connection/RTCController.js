import EventEmitter from 'events';
import { ACCEPT, READY, ANSWER, OFFER, CANDIDATE, sendReady, sendCandidate, sendOfferDescription, sendAnswerDescription } from '../../events';
import RTCConnection from './PeerConnection/RTCConnection';
import DeviceManager from '../../Devices';
import MessageChannel from '../../MessageChannel';

class ConnectionController extends EventEmitter {

	constructor() {
		super();
		this.localMediaStream = null;
		this.connection = null;

		this.onIceCandidate = (candidate, index, mediaId) => {
			console.log('send candidate', candidate, index, mediaId);
			sendCandidate(candidate, index, mediaId);
		}
		this.onAddRemoteStream = (stream) => {
			console.log('add stream', stream);
			this.emit('addstream', stream)
		}

		this.onChangeLocalStream = () => DeviceManager.getMediaStream()
			.then((stream) => this.changeStream(stream))

		DeviceManager.onVideoChange(this.onChangeLocalStream);
		DeviceManager.onAudioChange(this.onChangeLocalStream);
	}

	onAddStream(cb) {
		this.on('addstream', cb);
	}

	createConnection(stream) {
		if (!this.connection || !this.isConnected()) {
			this.connection = new RTCConnection(stream);
			window.connection = this.connection;
			this.connection.addStream(stream);
			this.connection.onIceCandidate(this.onIceCandidate);
			this.connection.onAddRemoteStream(this.onAddRemoteStream);
		}
		return this;
	}

	closeConnection() {
		if (this.connection) {
			this.connection.destroy();
			this.connection = null;
		}

	}

	setLocalStream(stream) {
		if (this.isConnected()) {
			this.connection.removeStream(this.localMediaStream);
			this.localMediaStream = stream;
			this.connection.addStream(stream);
			this.createAndSendOffer();
		}
		this.localMediaStream = stream;
		return this;
	}

	get connectionState() {
		return this.connection && this.connection.connectionState;
	}

	changeBandWidth(bandWidth) {
		this.connection.bandwidth = bandWidth;
		return this.createAndSendOffer()
	}

	changeStream(stream) {
		if(!this.connection) {
			this.setLocalStream(stream);
		} else {
			this.connection.changeStream(stream);
			this.createAndSendOffer()
		}
	}

	createAndSendOffer() {
		return this.connection.createOffer()
			.then((offerDescription) => sendOfferDescription(offerDescription));
	}

	createAndSendAnswer() {
		return this.connection.createAnswer()
			.then((offerDescription) => sendAnswerDescription(offerDescription));
	}

	setRemoteDescription(sessionDescription) {
		return this.connection.setRemoteDescription(sessionDescription);
	}

	setOfferDescription(sessionDescription) {
		return this.setRemoteDescription(sessionDescription)
			.then(() => this.createAndSendAnswer())
	}

	setAnswerDescription(sessionDescription) {
		return this.setRemoteDescription(sessionDescription);
	}

	addIceCandidate(candidate, sdpMid, sdpMLineIndex) {
		console.log('save candidate', candidate, sdpMid, sdpMLineIndex);
		this.connection.addIceCandidate({
			sdpMLineIndex,
			candidate,
			sdpMid
		});
	}

}


const connectionController = new ConnectionController();

MessageChannel.on(CANDIDATE, ({ candidate, index, mediaId }) => {
	connectionController.addIceCandidate(candidate, mediaId, index);
})

MessageChannel.on(OFFER, ({ sessionDescription }) => {
	connectionController.setOfferDescription(sessionDescription);
})

MessageChannel.on(ANSWER, ({ sessionDescription }) => {
	connectionController.setAnswerDescription(sessionDescription);
})


export default connectionController;

