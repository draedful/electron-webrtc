// @flow
import EventListener from 'events';

import { desktopCapturer, screen } from 'electron';

const getAllDevices = () => navigator.mediaDevices.enumerateDevices().then((devices) => {
	return new Promise((resolve, reject) => {
		desktopCapturer.getSources({ types: ['window', 'screen'] }, (error, sources) => {
			if (error) throw error;
			sources.forEach((screen) => {
				devices.push({
					type: 'screen',
					kind: 'videoinput',
					deviceId: screen.id,
					label: screen.name,
					preview: screen.thumbnail
				});
			});
			resolve(devices);
		}, reject);
	})
})

const getSize = () => screen.getPrimaryDisplay().workAreaSize;

const AUDIO_INPUT_KIND = 'audioinput';
const AUDIO_OUTPUT_KIND = 'audioinput';
const VIDEO_INPUT_KIND = 'videoinput';
const VIDEO_OUTPUT_KIND = 'videoinput';

const isDevice = (device) => !!(device.kind && device.deviceId);

class DeviceManager extends EventListener {

	constructor() {
		super();
		this.devices = new Map();

		this.currentAudioDeviceId = null;
		this.currentVideoDeviceId = null;

		navigator.mediaDevices.addEventListener('devicechange', () => this.fetchDevices().then(() => this.emit('UPDATE')));
	}

	getDevices() {
		if (this.devices.size) {
			return Promise.resolve(this.devices);
		}
		return this.fetchDevices();
	}

	getVideoDevices() {
		return this.getDevices()
			.then((devices) => {
				const video = [];
				devices.forEach((device) => {
					if (device.kind === VIDEO_INPUT_KIND) {
						video.push(device);
					}
				});
				return video;
			})
	}

	getAudioDevices() {
		return this.getDevices()
			.then((devices) => {
				const audio = [];
				devices.forEach((device) => {
					if (device.kind === AUDIO_INPUT_KIND) {
						audio.push(device);
					}
				});
				return audio;
			})
	}

	getVideoDeviceId() {
		if (this.currentVideoDeviceId && this.devices.has(this.currentVideoDeviceId)) {
			return Promise.resolve(this.currentVideoDeviceId);
		}
		return this.fetchDevices().then(() => this.currentVideoDeviceId);
	}

	getVideoDevice() {
		return this.getVideoDeviceId()
			.then(id => this.devices.get(id));
	}


	getAudioDeviceId() {
		if (this.currentAudioDeviceId && this.devices.has(this.currentAudioDeviceId)) {
			return Promise.resolve(this.currentAudioDeviceId);
		}
		return this.fetchDevices().then(() => this.currentAudioDeviceId);
	}

	getAudioDevice() {
		return this.getAudioDeviceId()
			.then(id => this.devices.get(id));
	}

	setVideoOutputDevice(device) {
		let videoDevice;
		if (device === null) {
			this.currentVideoDeviceId = null;
			this.emit('VIDEO_UPDATE');
		} else {
			if (isDevice(device) && this.devices.has(device.deviceId)) {
				videoDevice = device;
			} else if (this.devices.has(device)) {
				videoDevice = this.devices.get(device);
			}
			if (videoDevice) {
				this.currentVideoDeviceId = videoDevice.deviceId;
				this.emit('VIDEO_UPDATE');
			}
		}
	}

	setAudioOutputDevice(device) {
		let audioDevice;
		if (device === null) {
			this.currentAudioDeviceId = null;
			this.emit('AUDIO_UPDATE');
		} else {
			if (isDevice(device) && this.devices.has(device.deviceId)) {
				audioDevice = device;
			} else if (this.devices.has(device)) {
				audioDevice = this.devices.get(device);
			}
			if (audioDevice) {
				this.currentAudioDeviceId = audioDevice.deviceId;
				this.emit('AUDIO_UPDATE');
			}
		}

	}

	hasVideo() {
		return this.getVideoDeviceId().then((deviceId) => !!deviceId);
	}

	hasAudio() {
		return this.getAudioDeviceId().then((deviceId) => !!deviceId);
	}

	fetchDevices() {
		return getAllDevices()
			.then((devices) => {
				devices.forEach((device) => this.devices.set(device.deviceId, device));
				return devices;
			})
			.then((devices) => {
				if (!this.currentAudioDeviceId || !this.devices.has(this.currentAudioDeviceId)) {
					const audioDevice = devices.find((device) => device.kind === AUDIO_INPUT_KIND);
					if (audioDevice) {
						this.currentAudioDeviceId = audioDevice.deviceId;
					}
				}

				if (!this.currentVideoDeviceId || !this.devices.has(this.currentVideoDeviceId)) {
					const videoDevice = devices.find((device) => device.kind === VIDEO_INPUT_KIND);
					if (videoDevice) {
						this.currentVideoDeviceId = videoDevice.deviceId;
					}
				}

			})
			.then(() => this.devices);
	}

	getMediaStreamFrom(devicesId) {
		return devicesId.map((config, deviceId) => {
			if(this.devices.has(deviceId)) {
				const device = this.devices.get(deviceId);
				if(device.kind === AUDIO_INPUT_KIND) {

				} else if(device.kind === VIDEO_INPUT_KIND) {

				}
			}
			
			return config;
		})
	}

	getMediaStream(videoConfig = {}, audioConfig = {}) {
		return this.getDevices()
			.then(() => {
				if (this.currentAudioDeviceId) {
					audioConfig.deviceId = { exact: this.currentAudioDeviceId };
					if (this.currentVideoDeviceId) {
						const device = this.devices.get(this.currentVideoDeviceId);
						if (device.type === 'screen') {
							audioConfig = {
								mandatory: {
									chromeMediaSource: 'desktop'
								}
							};
							const minSize = device.preview.getSize();
							const maxSize = getSize();
							videoConfig = {
								mandatory: {
									chromeMediaSource: 'desktop',
									chromeMediaSourceId: device.deviceId,
									minWidth: minSize.width,
									maxWidth: maxSize.width,
									minHeight: minSize.height,
									maxHeight: maxSize.height
								}
							}
						} else {
							videoConfig.deviceId = { exact: this.currentVideoDeviceId };
						}
					}
					const MediaConfig = {
						audio: audioConfig,
						video: this.currentVideoDeviceId ? videoConfig : false
					};

					return navigator.mediaDevices.getUserMedia(MediaConfig)
				}
				return Promise.reject();
			});
	}

	onAudioChange(cb) {
		this.on('AUDIO_UPDATE', cb);
	}

	onVideoChange(cb) {
		this.on('VIDEO_UPDATE', cb);
	}

	onUpdate(cb) {
		this.on('UPDATE', cb);
	}

}/**/
import DeviceMana from './services/DeviceManager';
export default DeviceMana;

// const getDeviceStream = (device) => {
// 	return navigator.mediaDevices.getUserMedia({
// 		video: {
// 			deviceId: { exact: device.deviceId }
// 		}
// 	})
// };

// Promise.all(video.map((device) => getDeviceStream(device)))
// 	.then((streams) => {
// 		const mainStream = streams[0];
// 		streams[1].getTracks().forEach((track) => mainStream.addTrack(track));
// 		return mainStream;
// 	}).then((stream) => {debugger;})
// 	.catch(e => console.log(e));