import Select from '../../CoreComponents/Select';
import DeviceManager from '../../Services/DeviceManager';

export default class LocalCameraSelector extends Select {

  constructor(node) {
    super(node);
    this.updateDevices();
    DeviceManager.onUpdate(() => this.updateDevices());
  }

  updateDevices() {
    window.requestAnimationFrame(() => {
      DeviceManager.getVideoDevices()
        .then((cameras) => {
          this.clear();
          this.addOption('Выключить камеру', 'null');
          cameras.forEach((camera) => {
            this.addOption(camera.label, camera.deviceId);
          }, this)
        )
    });
  }

}