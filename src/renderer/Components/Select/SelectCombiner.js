import SelectEmitter, { SELECT } from './SelectEmitter';


export default class SelectCombiner extends SelectEmitter {
  
  constructor(...selectors) {
    this.selector = new Set(selectors);
    selectors.forEach((selector) => {
      selector.onChange((value) => this.emitSelect(selector, value));
    });
  }
  
}