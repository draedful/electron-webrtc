import VideoEmitter from './VideoEmitter';

const once = (node, event, cb) => {
	const resolver = (e) => {
		node.removeEventListener(event, resolver);
		cb(e);
	};
	node.addEventListener(event, resolver);
};

export default class Video extends VideoEmitter {

	constructor(node) {
		super();
		this.videoNode = node;
	}

	onVideoNodeEvent(event, cb) {
		this.videoNode.addEventListener(event, cb);
		return this;
	}

	offVideoNodeEvent(event, cb) {
		this.videoNode.removeEventListener(event, cb);
		return this;
	}

	setStream(stream) {
		return new Promise((resolve, reject) => {
			if (!stream || !(stream instanceof MediaStream)) {
				return reject(new Error('stream is not a MediaStream'));
			}
			this.videoNode.srcObject = stream;
			once(this.videoNode, 'loadeddata', resolve);
		}).then(() => {
			this.emitChange();
		})

	}

	setTrack(track) {
		if (!track || !(track instanceof MediaStreamTrack)) {
			throw new Error('stream is not a MediaStream');
		}
		const stream = new MediaStream();
		stream.addTrack(track);
		return this.setStream(stream);
	}

	setOutput(deviceId) {
		if (deviceId) {
			this.videoNode.setSinkId(deviceId);
		}
		return this;
	}

}