/* global proccess */
console.log('preload script');

if (process.type === 'renderer') {
	const { ipcRenderer } = require('electron');
	window.onMainMessage = (cb) => {
		ipcRenderer.on('message', (e, message) => cb(message));
	}

	window.sendMainMessage = (message) => {
		ipcRenderer.send('message', message);
	}

	window.changeSize = (width, height) => {
		console.log('change size', width, height);
		ipcRenderer.send('manage', {
			type: 'setSize',
			width,
			height,
			aspectRatio: width / height
		})
	};
}
