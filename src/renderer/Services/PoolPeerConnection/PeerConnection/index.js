import EventEmitter, {
	ICE_CANDIDATE,
	CHANGE_STREAM,
	CLOSE,
	CONNECT
} from './ConnectionEventEmitter';

const RTCPeerConnection = window.mozRTCPeerConnection || window.webkitRTCPeerConnection || window.RTCPeerConnection;
const IceCandidate = window.mozRTCIceCandidate || window.RTCIceCandidate;
const SessionDescription = window.mozRTCSessionDescription || window.RTCSessionDescription;

const createOfferMandatory = (offerToReceiveVideo = true, offerToReceiveAudio = true, voiceActivityDetection = true) => ({
	offerToReceiveVideo,
	offerToReceiveAudio,
	voiceActivityDetection
});

// The current state of the ICE agent and its connection
//  https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/iceconnectionstate
const NEW_ICE_STATE = 'new';
const CHECKING_ICE_STATE = 'checking';
const CONNECTED_ICE_STATE = 'connected';
const COMPLETED_ICE_STATE = 'completed';
const FAILED_ICE_STATE = 'failed';
const DISCONNECTED_ICE_STATE = 'disconnected';
const CLOSED_ICE_STATE = 'closed';

const CLOSED_CONNECTION = [FAILED_ICE_STATE, DISCONNECTED_ICE_STATE, CLOSED_ICE_STATE];
const CONNECTED_CONNECTION = [CONNECTED_ICE_STATE, COMPLETED_ICE_STATE];

export default class PeerConnection extends EventEmitter {

	constructor(networkConfig) {
		super();
		if (!networkConfig) {
			throw new Error('Network Setting should be Object');
		}
		this.connection = new RTCPeerConnection(networkConfig);

		this._onConnectionStateHandler = (e) => {
			const iceConnectioState = e.target.iceConnectionState;
			if (CLOSED_CONNECTION.includes(iceConnectioState)) {
				return this.emit(CLOSE);
			}
			if (CONNECTED_CONNECTION.includes(iceConnectioState)) {
				return this.emit(CONNECT);
			}
		};
		this._onAddStreamHandler = (e) => this.emit(CHANGE_STREAM, e.stream);
		this._onIceCandidateHandler = (e) => e.candidate && this.emit(ICE_CANDIDATE, e.candidate);

		this.connection.addEventListener('icecandidate', this._onIceCandidateHandler);
		this.connection.addEventListener('addstream', this._onAddStreamHandler);
		this.connection.addEventListener('iceconnectionstatechange', this._onConnectionStateHandler);
	}

	get localStream() {
		return this.connection.getLocalStream();
	}

	get remoteStream() {
		return this.connection.getRemoteStream();
	}

	changeStream(stream) {
		if (!stream || !(stream instanceof MediaStream)) {
			throw new Error('Local Stream should be MediaStream Object');
		}

		const locaStream = this.localStream;
		if (locaStream) {
			this.connection.remoteStream(localStream);
			this.connection.addStream(stream);
			return this.createOffer();
		} else {
			this.connection.addStream(stream);
		}
		return Promise.resolve();
	}

	createOffer(mandatory) {
		return this.connection.createOffer()
			.then((offerSDP) => this.connection.setLocalDescription(offerSDP));
	}

	createAnswer() {
		return this.connection.createAnswer()
			.then((answerSDP) => this.connection.setLocalDescription(answerSDP));
	}

	setOfferDescription(offerDescription) {
		return this.connection.setRemoteDescription(offerDescription)
			.then(() => this.createAnswer());
	}

	close() {
		this.connection.removeEventListener('icecandidate', this._onIceCandidateHandler);
		this.connection.removeEventListener('addstream', this._onAddStreamHandler);
		this.connection.removeEventListener('iceconnectionstatechange', this._onConnectionStateHandler);
		this.connection.removeStream(this.localStream);
		this.connection.close();
		this.connection = null;
		super.destroy();
	}

}