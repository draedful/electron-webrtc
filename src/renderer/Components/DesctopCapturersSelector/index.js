import Select from '../../CoreComponents/Select';
import DeviceManager from '../../Services/DeviceManager';
import groupBy from 'lodash/groupBy';
import forEach from 'lodash/forEach';

export default class DesctopCaprurerSelector extends Select {

	constructor(node) {
		super(node);
		this.updateDevices();
		DeviceManager.onUpdate(() => this.updateDevices());
	}

  updateDevices() {
    window.requestAnimationFrame(() => {
      DeviceManager.getScreens()
        .then((cameras) => {
          this.clear();
          this.addOption('Отключено', 'null');
          const groups = groupBy(cameras, (camera) => camera.id.split(':')[0]);
          forEach(groups, (devices, devicesType) => this.addGroup(devicesType, devices));
        );
    });
  }

	addGroup(name, devices) {
		const group = `<optgroup label='${name}'>` +
			devices.map((device) => (`<option value='${device.id}'>` + device.name + `</option>`)).join('') +
			`</optgroup>`;
		this.selectorNode.innerHTML = this.selectorNode.innerHTML + group;
	}

}