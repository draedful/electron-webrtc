


export default class VideoController {

	constructor(node) {
		this.videoNode = node;
	}

	stopCurrentStream() {
		if(this.videoNode.srcObject) {
			this.videoNode.srcObject.getTracks()
				.forEach(track => track.stop());
		}
	}

	setStream(stream) {
		this.stopCurrentStream();
		this.videoNode.srcObject = stream;
	}

}