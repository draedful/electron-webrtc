import PeerConnection from './Connection';

const NetworkConfig = {
	iceServers: [
		{ url: 'stun:stun.l.google.com:19302' },
		{ url: 'turn:turn.agmservice.com:3478', credential: 'my_password', username: 'my_username' }
	]
};

export default class PoolPeerConnection extends Map {

	constructor() {
		super();
		this.localStream = null;
	}

	setDefaultStream(stream) {
		if (!stream || !(stream instanceof MediaStream)) {
			throw new Error('First argument should be MediaStream');
		}
		this.localStream = stream;
		return this;
	}

	createConnection(connectionId) {
		if (!this.localStream) {
			throw new Error('local stream required for creating PeerConnection');
		}
		const connection = new PeerConnection(NetworkConfig);
		this.set(connectionId, connection);
		return connection.changeStream(this.localStream);
	}


}