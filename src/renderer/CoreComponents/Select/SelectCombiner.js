import SelectEmitter from './SelectEmitter';


export default class SelectorCombiner extends SelectEmitter {

	constructor(...selectors) {
		super();
		this.selectors = new Set(selectors);
		this._emit = () => this.emitSelect();
		selectors.forEach((selector) => this.addSelector(selector));
	}

	getValues() {
		const accum = new Array(this.selectors.size);
		let index = 0;
		this.selectors.forEach((value) => {
			accum[index++] = value.getSelected();
		});
		return accum;
	}

	addSelector(selector) {
		if (selector instanceof SelectEmitter) {
			selector.onSelect(this._emit);
		}
	}

	removeSelector(selector) {
		if (selector instanceof SelectEmitter) {
			selector.offSelect(this._emit);
		}
		this.selectors.delete(selector);
	}

}