import EventEmitter from 'events';
import BandwidthHandler from './BandWidth';

const PeerConnection = window.mozRTCPeerConnection || window.webkitRTCPeerConnection || window.RTCPeerConnection;
const IceCandidate = window.mozRTCIceCandidate || window.RTCIceCandidate;
const SessionDescription = window.mozRTCSessionDescription || window.RTCSessionDescription;

const NetworkConfig = {
	iceServers: [
		{ url: 'stun:stun.l.google.com:19302' },
		{ url: 'turn:turn.agmservice.com:3478', credential: 'my_password', username: 'my_username' }
	]
};

const NEW_CONNECTION = 'new';
const CONNECTION = 'connecting';
const CONNECTED = 'connected';

// TODO интеграция шифрования потока https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/generateCertificate

export default class RTCConnection extends EventEmitter {

	/**
	 * @param {MediaStream} stream
	 * @param {number} [bandwidth = null]
	 * */
	constructor(stream, bandwidth = null) {
		super();
		/**
		 * @type {RTCPeerConnection}
		 * */
		this.connection = new PeerConnection(NetworkConfig);

		this.bandwidth = bandwidth;

		this.onReceiveIceCandidate = (event) => {
			if (event.candidate) {
				const candidate = event.candidate;
				this.emit('icecandidate', candidate.candidate, candidate.sdpMLineIndex.toString(), candidate.sdpMid);
			}
		};

		this.onAddStream = (event) => {
			console.log('add stream');
			this.removeStream = event.stream;
			this.emit('addstream', this.removeStream);
		}

		
		this.connection.addEventListener('addstream', this.onAddStream);
		this.connection.addEventListener('icecandidate', this.onReceiveIceCandidate);
	}
  
	removeStream() {
		this.connection.removeStream(this.localStream);
	}

	/**
	 * @param {MediaStream} stream
	 *
	 * */
	addStream(stream) {
		this.localMediaStream = stream;
		this.connection.addStream(this.localMediaStream);
		return this;
	}

	onAddRemoteStream(cb) {
		this.on('addstream', cb);
	}

	onIceCandidate(cb) {
		this.on('icecandidate', cb);
	}

	onNativeEvent(event, cb) {
		this.connection.addEventListener(event, cb);
	}

	isConnected() {
		return this.connection.connectionState === CONNECTED;
	}

	modifySessionDescription(sdp) {
		return this.bandwidth ? new SessionDescription({
			type: sdp.type,
			sdp: BandwidthHandler.setVideoBitrates(sdp.sdp, {
				min: this.bandwidth,
				max: this.bandwidth
			})
		}) : sdp;
	}

	createOffer() {
		return this.connection.createOffer()
			.then((localDescription) => this.modifySessionDescription(localDescription.toJSON()))
			.then((localDescription) => this.setLocalDescription(localDescription));
	}

	createAnswer() {
		return this.connection.createAnswer()
			.then((localDescription) => this.modifySessionDescription(localDescription))
			.then((localDescription) => this.setLocalDescription(localDescription))
	}

	setRemoteDescription(sessionDescription) {
		return this.connection.setRemoteDescription(new SessionDescription(sessionDescription));
	}

	setLocalDescription(description) {
		return this.connection.setLocalDescription(description)
			.then(() => description);
	}

	addIceCandidate(iceDscription) {
		this.connection.addIceCandidate(new IceCandidate(iceDscription));
		return this;
	}

	changeStream(stream) {
		this.connection.removeStream(this.localMediaStream);
		this.localMediaStream = stream;
		this.connection.addStream(this.localMediaStream);
		return this;
	}

	destroy() {
		this.connection.stop();
		this.connection.removeEventListener('addstream', this.onReceiveIceCandidate);
		this.connection.removeEventListener('icecandidate', this.onReceiveIceCandidate);
	}

}