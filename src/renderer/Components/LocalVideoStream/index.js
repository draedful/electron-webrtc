import { LocalStreamManager } from '../../Services/StreamManager';
import VideoWrapper from './VideoWrapper';

const HIDE_LOCAL_VIDEO = 'local-video-hide';

const markupContent = `<div class="video-content">
	<video autoplay muted></video>
	<div class="video-stub">
	<div class="b-circle-preloader-r14"></div>
</div>
</div>`;

export default class LocalVideoStream {

	constructor(node) {
		this.videoWrapperNode = node;
		this.videoContainers = [];
		LocalStreamManager.onChange((stream) => this.setStream(stream));
	}

	/**
	 * @param {MediaStream} stream
	 * */
	setStream(stream) {
		if (!stream || !(stream instanceof MediaStream)) {
			throw new Error('stream should be MediaStream');
		}
		const videoTracks = stream.getVideoTracks();
		if (videoTracks.length) {
			this.videoContainers.length = videoTracks.length;
			videoTracks.forEach((video, index) => {
				let container = this.videoContainers[index];
				if (!container) {
					container = VideoWrapper.createVideoWrapper(this.videoWrapperNode, markupContent, 'video-content-wrapper');
					this.videoContainers.push(container);
				}
				container.setTrack(video);
			});
		}
	}

}