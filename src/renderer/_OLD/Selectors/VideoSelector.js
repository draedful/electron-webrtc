import DeviceManager from '../../Devices';
import DeviceSelector from './Selector';

export default class VideoSelector extends DeviceSelector {

	constructor(node) {
		super(node);
		DeviceManager.onUpdate(() => this.updateVideoDevices());
	}

	onSelect(deviceId) {
		DeviceManager.setVideoOutputDevice(deviceId === 'off_video' ? null : deviceId);
		super.onSelect(deviceId);
	}

	updateVideoDevices() {
		return DeviceManager.getVideoDevices()
			.then(devices => {
				this.updateDevicesView(devices);
				return devices;
			})
			.then(() => DeviceManager.getVideoDevice())
			.then((device) => this.setSelectedDevice(device))
	}

}