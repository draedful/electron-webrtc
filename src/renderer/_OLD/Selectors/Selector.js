

export default class DeviceSelector {

	constructor(node) {
		this.nodeDeviceSelector = node;
		this.nodeDeviceSelector.onchange = (e) => this.onSelect(e.target.value);
	}

	onSelect(deviceId) {
		this.selectedDeviceId = deviceId;
		console.log('select device', deviceId)
	}

	getSelector() {
		return this.nodeDeviceSelector;
	}

	updateDevicesView(devices) {
		devices.forEach((device) => this.addDevice(device));
	}

	setSelectedDevice(device) {
		this.selectedDeviceId = device.deviceId;
		const option = this.nodeDeviceSelector.querySelector(`[value='${device.deviceId}']`);
		if (option) {
			option.selected = true;
		}
		return this;
	}

	addDevice(device) {
		const option = new Option(device.label, device.deviceId);
		if(device.deviceId === this.selectedDeviceId) {
			option.selected = true;
		}
		this.nodeDeviceSelector.options.add(option);
		return this;
	}

	clear() {
		while(this.nodeDeviceSelector.options.length) {
			this.nodeDeviceSelector.options.remove(0);
		}
	}

}