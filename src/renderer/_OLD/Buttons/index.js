import { sendInvite, acceptCall, INVITE } from '../../events';
import MessageChannel from '../../MessageChannel';

let intiveBtn;
let acceptBtn;

export const inviteButton = (node) => {
	intiveBtn = node;
	node.addEventListener('click', (e) => {
		node.disabled = true;
		sendInvite();
	});
};
export const acceptButton = (node) => {
	acceptBtn = node;
	node.addEventListener('click', acceptCall);
};

MessageChannel.on(INVITE, () => {
	intiveBtn.disabled = true;
	acceptBtn.disabled = false;
});



