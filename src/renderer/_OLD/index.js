import VideoSelector from './Selectors/VideoSelector';
import AudioInputSelector from './Selectors/AudioInputSelector';
import LocalVideoController from './Video/LocalVideoController';
import RemoteVideoControler from './Video/RemoteVideoController';
import LocalScreenController from './Selectors/ScreenSelector';


import { inviteButton, acceptButton } from './Buttons/index';


const audioInput = new AudioInputSelector(document.querySelector('#audioInput'));
const videoSelector = new VideoSelector(document.querySelector('#videoInput'));
const screenController = new LocalScreenController(document.querySelector('#screenInput'));

Promise.all([
	videoSelector.updateVideoDevices(),
	audioInput.updateAudioDevices()
])
	.then(() => {
		const localVideo = new LocalVideoController(document.querySelector('#localVideo'));
		return localVideo.loadStream();
	});

inviteButton(document.querySelector('#inviteCall'));
acceptButton(document.querySelector('#acceptCall'));

const remoteVideo = new RemoteVideoControler(document.querySelector('#remoteVideo'));

const audioOutput = document.querySelector('#audioOutput');
audioOutput.onchange = (e) => remoteVideo.setAudioOutput(e.target.value);

const bandwidth = document.querySelector('#bandwidth');
bandwidth.onchange = (e) => remoteVideo.setBandWidth(+e.target.value);
