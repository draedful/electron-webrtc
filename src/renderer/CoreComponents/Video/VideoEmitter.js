import EventEmitter from 'events';

export const CHANGE = Symbol('change');

export default class VideoEmitter extends EventEmitter {

	emitChange() {
		return this.emit(CHANGE);
	}

	onChange(cb) {
		return this.on(CHANGE, cb);
	}

	offChange(cb) {
		return this.off(CHANGE, cb);
	}

}