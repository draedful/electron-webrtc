import RTCController from '../Connection/RTCController';
import VideoController from './VideoController';


export default class RemoteVideoController extends VideoController {

    constructor(videoNode) {
        super(videoNode);
        RTCController.onAddStream((stream) => this.setStream(stream));
    }

    setBandWidth(bandwidth) {
        RTCController.changeBandWidth(bandwidth);
        return this;
    }

    setAudioOutput(deviceId) {
        this.videoNode.setSinkId(deviceId);
        return this;
    }

}